# Changelog

## 1.0.0 (2024-10-07)


### Features

* change appversion to 23.05.0.5.1 ([d2e9c7d](https://gitlab.mim-libre.fr/EOLE/eole-3/services/collabora/collabora-helm-chart/commit/d2e9c7d6803aa0950163f5d55b7dd2532cfae020))
* initial release ([c7bc173](https://gitlab.mim-libre.fr/EOLE/eole-3/services/collabora/collabora-helm-chart/commit/c7bc1738e56e799ce9d473004e8d3e80cbf51f41))
* the chart name is collabora ([5cd382f](https://gitlab.mim-libre.fr/EOLE/eole-3/services/collabora/collabora-helm-chart/commit/5cd382f56f44b5c9d3b0bb32f34622c93f74b8bd))
* update chart and app version 23.05.2.2.1 ([6f8da26](https://gitlab.mim-libre.fr/EOLE/eole-3/services/collabora/collabora-helm-chart/commit/6f8da26e961ab1a8a00f4779414f4c2db4a2387b))
* update helm chart to 1.1.22 ([ed59906](https://gitlab.mim-libre.fr/EOLE/eole-3/services/collabora/collabora-helm-chart/commit/ed599065dcaf2d5f07d33ec4db939fc9bfbd372e))


### Bug Fixes

* rename helm chart to collabora ([c2131b3](https://gitlab.mim-libre.fr/EOLE/eole-3/services/collabora/collabora-helm-chart/commit/c2131b3f72bdbe25424dc4f4f4857a2a6ae0cf78))
* update app version 22.05.14.3.1 ([bff73a9](https://gitlab.mim-libre.fr/EOLE/eole-3/services/collabora/collabora-helm-chart/commit/bff73a9923372c22166820088e89f10c30409513))
* update dependencies images and chart name ([eb27787](https://gitlab.mim-libre.fr/EOLE/eole-3/services/collabora/collabora-helm-chart/commit/eb277871a580c80d1f4bbddeb51a376b18c97e5c))
